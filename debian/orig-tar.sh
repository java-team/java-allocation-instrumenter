#!/bin/sh

VERSION=$2
DIR=java-allocation-instrumenter-$VERSION
TAR=../java-allocation-instrumenter_$VERSION.orig.tar.xz

rm $3

svn export http://java-allocation-instrumenter.googlecode.com/svn/tags/java-allocation-instrumenter-$VERSION $DIR

tar -c -J -f $TAR $DIR
rm -Rf $DIR
