Source: java-allocation-instrumenter
Section: java
Priority: optional
Maintainer: Debian Java maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Tim Potter <tpot@hp.com>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk-headless | default-jdk,
 javahelper,
 junit,
 libasm-java,
 libbuild-helper-maven-plugin-java,
 libguava-java,
 libjarjar-maven-plugin-java,
 libmaven-source-plugin-java,
 libmaven-shade-plugin-java,
 libreplacer-java,
 maven-debian-helper
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/java-allocation-instrumenter.git
Vcs-Browser: https://salsa.debian.org/java-team/java-allocation-instrumenter
Homepage: https://github.com/google/allocation-instrumenter

Package: libjava-allocation-instrumenter-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: JVM agent tracking memory allocations
 Java Allocation Instrumenter is a Java agent written using the
 java.lang.instrument API. Each allocation in your Java program is instrumented;
 a user-defined callback is invoked on each allocation.
 .
 Bytecode rewriting is used to invoke the callback at the site of each
 allocation.
